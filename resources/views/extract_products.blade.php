<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #000000;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            /*height: 100vh;*/
            margin: 0;
        }




    </style>
</head>
<body>
<div class="flex-center position-ref full-height">
    @if (Route::has('login'))
        <div class="top-right links">
            @auth
                <a href="{{ url('/home') }}">Home</a>
            @else
                <a href="{{ route('login') }}">Login</a>
                <a href="{{ route('register') }}">Register</a>
            @endauth
        </div>
    @endif

    <div  style="margin-top: 100px">
        <div class="col">
            <table class="table table-hover table-bordered table-dark">
                <th>Title</th>
                <th>Body</th>
                <th>vendor</th>
                <th>Tags</th>

            @foreach($data as $key=>$value)
                <tr >
                <td>{{$value->title}}</td>
                    @php
                        $string=strip_tags($value->body);
   $string = str_replace("&nbsp;", "", $string);
   $body = trim(preg_replace('/\s+/', ' ', $string));
                            @endphp
                    <td>{{@$body}}</td>
                    <td>{{@$value->vendor}}</td>
                    <td>{{@$value->tags}}</td>

                </tr>
                @endforeach
            </table>
        </div>


    </div>
</div>
</body>
</html>
