<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Foundation\Http\FormRequest;



class ApiServicesController extends Controller
{
    //

    protected $shopify_url='https://e8fe1882da2563489067f68a20a5d716:94ec0b61a90951a56de989f8ca32da8b@sellenvo.myshopify.com';

    protected function performRequest($method,$url,$parameters=[]){

        set_time_limit(0);
        $client = new Client();

        $response = $client->request($method,$url, ['headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json'], 'body' =>json_encode($parameters)]
        );

        return $response->getBody()->getContents();

    }

///////////////////////////////////////////////////////////////////////////////////
    protected function performGetRequest($url){
//     dd($url);
        $client = new Client();
        $contents = $client->request('GET',$url);

        $response=$contents->getBody()->getContents();
        //$contents= $this->performRequest('GET',$url);
        $decodeContent= json_decode($response,true);

        return $decodeContent;
    }
///////////////////////////////
    protected function performPostRequest($url,$parameters){
        //dd(json_encode($parameters));

        $contents= $this->performRequest('POST',$url,$parameters);

        $decodeContent= json_decode($contents);
        //dd($decodeContent);
// if($decodeContent){
//     return true;
// }
     //  return $decodeContent->product->title;
    }
    /**********************************/
///////////////////////////////
    protected function performPutRequest($url,$parameters){

//dd($data );
        $contents= $this->performRequest('PUT',$url,$parameters);

        $decodeContent= json_decode($contents);
// dd($decodeContent);
        return $decodeContent;
    }
    /**********************************/
///////////////////////////////
    protected function performDeleteRequest($url,$parameters){

//dd($data );
        $contents= $this->performRequest('Delete',$url,$parameters);

        $decodeContent= json_decode($contents);
// dd($decodeContent);
        return $decodeContent->data;
    }
    /**********************************/


}
