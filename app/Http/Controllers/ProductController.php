<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Excel;
use File;


class ProductController extends ApiServicesController
{
    //

    public function extractData(){
        $filename = storage_path('products_export.csv');

        $data = Excel::load($filename, function($reader) {
        })->get();


        return view('extract_products', compact('data'));


    }

    public function createProduct(){

        $filename = storage_path('products_export.csv');

        $data = Excel::load($filename, function($reader) {
        })->get();


            foreach ($data as $key => $value) {

                $options = [
                 'product'=>[
                        'title' => $value->title,
                'body_html' => $value->body,
                'vendor' => $value->vendor,
                'tags' => $value->tags,
    ]
                ];

                $url=$this->shopify_url.'/admin/products.json';

             $this->performPostRequest($url,$options);


            }
        return response()->json('Products were added successfully', 201,[],JSON_NUMERIC_CHECK);

    }

    public function getProducts(){
        $url=$this->shopify_url.'/admin/api/2019-04/products.json?fields=title,body_html,vendor,tags';
        $data=$this->performGetRequest($url);
        $data=$data['products'];

        $headers=['Title','Body_html','Vendor','Tags'];

      Excel::create('all_products', function ($excel) use ($headers, $data) {

            // Build the spreadsheet, passing in the users array

              $excel->sheet('sheet1', function ($sheet) use ($data) {
                  $sheet->fromArray($data);
              });


        })->download('xlsx');
    }

    public function updateProducts(){

        $retrieve_product_url=$this->shopify_url.'/admin/api/2019-04/products.json?fields=id,title,variants';
        $data=$this->performGetRequest($retrieve_product_url);
        $data=$data['products'];

        $inventory_url=$this->shopify_url.'/admin/locations.json';
        $result=$this->performGetRequest($inventory_url);


foreach($result['locations'] as $location) {
    $location_id=$location['id'];

    foreach ($data as $key => $value) {

            $inventory_id=$value['variants'][0]['inventory_item_id'];

            $options = [
             'location_id'=>$location_id,
               'inventory_item_id'=>$inventory_id,
                'available'=>50
            ];
        $url = $this->shopify_url . '/admin/inventory_levels/set.json' ;

        $this->performPostRequest($url,$options);
    }
}
        return response()->json('Inventory quantity updated successfully', 201,[],JSON_NUMERIC_CHECK);

    }

}
